import express from 'express';
import path from 'path';
import { fileURLToPath } from 'url';
import mqtt from 'mqtt';

const app = express();

const client = mqtt.connect('mqtt://igbt.eesc.usp.br',
                            {
                                username: 'mqtt',
                                password: 'mqtt_123_abc'
                            });

const mqtt_topic = 'node';

client.on('message', (topic, message) => {
    console.log(`Received message on topic ${topic}: ${message}`);
});

client.on('connect', () => {
    console.log('Connected to MQTT broker');
    console.log('Subscribing to node topic');
    client.subscribe(mqtt_topic, (err) => {
        if (err) {
            console.error(`Error on subscribing to topic ${mqtt_topic}: ${err}`);
        }
        else {
            console.log(`Successfuly subscribed to topic: ${mqtt_topic}`);
        }
    });
});

app.set('view engine', 'pug');
app.set('views', '.');

app.get('/', (req, res) =>
	  res.render('index')
);

import fileUpload from 'express-fileupload';

// default options
app.use(fileUpload());

app.get('/send', function(req, res) {
    client.publish('node', 'Hello from NodeJS!', { qos: 1});
    console.log("Plublished");
    res.status(200).send('Ok');
});

app.post('/upload', function(req, res) {
    if (!req.files || Object.keys(req.files).length === 0) {
        return res.status(400).send('No files were uploaded.');
    }

    // console.log("req.files=%j", req.files);

    console.log(`Filename=${req.files.file1.name}`);

    res.status(200).send("Ok");
});


const __filename = fileURLToPath(import.meta.url);

const __dirname = path.dirname(__filename);

app.use(express.static(__dirname + '/public'));

app.listen(3000, () =>
	  console.log('Servidor iniciado na porta 3000')
);
